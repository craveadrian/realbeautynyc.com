<div id="welcome">
	<div class="row">
		<div class="wrapper">
			<div class="wlcLeft">
				<img src="public/images/content/welcome.jpg" alt="facial">
			</div>
			<div class="wlcRight">
				<h2>Welcome</h2>
				<p>
					Welcome to Real Beauty Facial and Waxing Center, the best beauty spa in New York City, NY. Do you want to look and feel your best? Do you want the services of professionals who you can trust to perform treatments correctly? We serve the Astoria, Stamford, New Rochelle, and the surrounding areas and we want to be the beauty spa you trust to keep you looking amazing. If you’re in the mood for the best facial service, let our talented staff help you select one that is perfect for your skin.
				</p>
				<a href="services#content" class="btn">LEARN MORE</a>
			</div>
		</div>
	</div>
</div>
<div id="what-we-do">
	<div class="row">
		<div class="wrapper">
			<div class="left">
				<div class="text">
					<h2> What <span>We Do</span> </h2>
					<p>We offer threading near me, waxing, best facial service, microblading and more to bring out your full potential.</p>
				</div>
			</div>
			<div class="mid">
				<dl>
					<dt> <img src="public/images/content/what-we-do1.jpg" alt="What We Do 1"> </dt>
					<dd>BEAUTY SPA</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/what-we-do2.jpg" alt="What We Do 2"> </dt>
					<dd>THREADING</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/what-we-do3.jpg" alt="What We Do 3"> </dt>
					<dd>WAXING</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/what-we-do4.jpg" alt="What We Do 4"> </dt>
					<dd>SUGARING</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/what-we-do5.jpg" alt="What We Do 5"> </dt>
					<dd>FACIALS</dd>
				</dl>
				<dl>
					<dt> <img src="public/images/content/what-we-do6.jpg" alt="What We Do 6"> </dt>
					<dd>HENNA TATTOOS</dd>
				</dl>
			</div>
			<div class="right"></div>
		</div>
	</div>
</div>
<div id="why-choose-us">
	<div class="row">
		<h3>Why Choose Us?</h3>
		<div class="wrapper">
			<div class="top">
				<div class="left">
					<p>We always make sure that the products we use are safe and will give you the results you’re looking for. We have years of experience performing a wide variety of beauty treatments and we care about making sure our customers are happy and feeling great when they leave. If you’re looking for hair removal, we offer a wide variety of options such as waxing and sugaring.</p>
				</div>
				<div class="right">
					<img src="public/images/content/why-choose-us.jpg" alt="eye lashes">
				</div>
			</div>
			<div class="bot">
				<p>CALL US TODAY TO SET AN APPOINTMENT <span><?php $this->info(["phone","tel"]); ?></span> </p>
			</div>
		</div>
	</div>
</div>
<div id="gallery">
	<div class="row">
		<div class="images">
			<img src="public/images/content/gallery1.jpg" alt="gallery image 1">
			<img src="public/images/content/gallery2.jpg" alt="gallery image 2">
			<img src="public/images/content/gallery3.jpg" alt="gallery image 3">
			<img src="public/images/content/gallery4.jpg" alt="gallery image 4">
		</div>
		<div class="right fr">
			<h2>Our Gallery</h2>
			<p>If you’re interested in threading near me, we offer that as well. ‘Threading near me’ is an effective hair removal process that often has less discomfort than other methods, so it is popular. Our experts can get your brows shaped up and looking the way you want in no time.</p>
			<a href="gallery#content" class="btn">VIEW MORE</a>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<div id="testimonials">
	<div class="row">
		<p class="sm">
			<a href="<?php $this->info("fb_link"); ?>">F</a>
			<a href="<?php $this->info("tt_link"); ?>">L</a>
			<a href="<?php $this->info("yt_link"); ?>">X</a>
			<a href="<?php $this->info("rs_link"); ?>">R</a>
		</p>
		<div class="container">
			<div class="left">
				<p>Trupti tinted and threaded my brows to perfection today! She also did a full facial wax (which I’ve never had done before). And of course, I kept asking “will it hurt?!” She was very patient with me, and provided great customer service. We had a nice little chat, and before I knew it, she was done! I’ll definitely be back soon!</p>
				<p class="auth">DANIELLE G.</p>
				<p class="stars">&#9733; &#9733; &#9733; &#9733; &#9733;</p>
				<a href="testimonials#content" class="btn">READ MORE</a>
			</div>
			<div class="right">
				<h2>What <span>They Say</span></h2>
				<img src="public/images/content/quote.png" alt="quote" class="quote">
			</div>
		</div>
	</div>
</div>
